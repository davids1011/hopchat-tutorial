//Use this function to add a message to the DOM. It does not save that message to your database.
function addMessageToPage(user, text) {
    $('#messages').append('<div>'+ user + ': ' + text +'</div>')
}

var username;
var usernameInput = $('#username-input');
var messageInput = $('#message-input');


//This listener handles username input.
usernameInput.keyup(function(event) {
    if (event.keyCode == 13) {
        username = usernameInput.val();
        $('#chat').show();
        $('#signin').hide();
    }
});


//This listener handles message input.
messageInput.keyup(function(event) {
    if (event.keyCode == 13) {
        $.post({
            url: '/message',
            dataType: 'json',
            data: {
                user: username,
                text: messageInput.val()
            }
        });
        messageInput.val('');
    }
});

//FIREBASE MESSAGE LISTENER GOES HERE