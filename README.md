# Instructions

## Prerequisites

  - [Node.js](https://nodejs.org/en/download/)
  - [Firebase account](https://firebase.google.com/)

## Get the code

1. Clone this repo `git clone git@bitbucket.org:stephen-bennett/hopchat-tutorial.git`

## Set up the project

1. `npm install`
1. `npm start`
1. open `http://localhost:3000/`

## Make a Firebase project

1. [Go to the Firebase console](https://console.firebase.google.com/)
1. Click "Create New Project" and enter a project name (e.g. hopchat). Leave the country/region as default. Click "Create Project".


## Configure Firebase

### Front-end client

1. Go to the dashboard for your Firebase project.
1. Click "Add Firebase to your web app" and copy the snippet of code.
1. Paste it inside `public/index.html` at the bottom of the body tag but before the other script tags (there's a comment indicating where it should go).

This loads the Firebase library and configures it to point at your Firebase project when someone loads your webpage.

### Backend server

1. In a terminal, `cd` to this project's root folder and run:
        `npm install --save firebase`
1. Go to the dashboard for your Firebase project.
1. Click the Settings cog -> Permissions.
1. Go to "Service accounts". Click "Create Service Account".
1. Pick a service account name (eg. "hopchat"), select the role Project > Owner, tick the "Furnish new private key" box and click "Create".
1. Move the downloaded file to the root folder of your chat project. Rename it to `service-account.json`.
1. Paste the following text near the top of your `app.js` file (there's a comment suggesting where) and fill in the missing information.
Your databaseURL is in the information you copied for the client configuration earlier.


        var firebase = require("firebase");
        firebase.initializeApp({
          serviceAccount: "<path to service account file (probably service-account.json)>",
          databaseURL: "<url to connect to your firebase database>"
        });


This creates a Service Account role which has permission to perform any action on your Firebase service and sets up your server to use that role. You want to keep the key file secret.


## Read messages from Firebase

Add the following code to the bottom of `public/javascripts/chat.js`.


        firebase.database().ref('messages').on('child_added', function(data) {
            var message = data.val();
            addMessageToPage(message.user, message.text);
        });


This creates a "child_added" listener at the path "messages" in your database. The callback you provide will be fired everytime a child is added at that position in your database by anyone.
`data` will contain information about that event, including the value that was added which you can access with `data.val()`.
So this listener is calling `addMessageToPage` with the user and text of each child added to the "messages" location in your database.
When this listener is registered it will be fired for every child currently at that position, and then again for every child that is added afterwards until you disable it.

## Authorise anyone to read

Go to "Develop > Database" in the Firebase console. Choose the "rules" tab, and change the database rules to the following:

        {
          "rules": {
            ".read": "true",
            ".write": "auth != null"
          }
        }
        
This will enable any client to read from the database.

## Write messages to Firebase

In `chat.js` you'll notice the message input handler has this snippet of code:

        $.post({
            url: '/message',
            dataType: 'json',
            data: {
                user: username,
                text: messageInput.val()
            }
        });


This is making a POST request to our server at the url "/message" with message data from the user.
Our express server needs a request handler for that url that will do whatever message processing we want done and then save the result to Firebase.
In `app.js` put the following snippet after line 23 (there's a comment indicating where).


        app.post('/message', function(req, res) {
          //Do whatever message processing you want here.
          firebase.database().ref('messages').push(req.body);
          res.send('ok');
        });


This snippet of code registers a post handler with the express server for the url `/message`. When a request comes to our server at that url the callback we provided will be fired.
The `req` object contains information about the request, and the `res` object provides methods for building the response.

In this case we're just directly saving the body of the request to Firebase. The `push` method creates a child at the location "messages" in the database.
When using this method the key of the child is automatically generated and will be greater than the previous key at the time of creation, so our children are kept in the same order they were created.

## Congratulations!

At this point you should have a chat application running locally that stores chat history in Firebase and updates in real time. If you open multiple tabs to `http://localhost:3000/` you should be able to have a conversation with yourself.

## Further exercises

Try doing some sort of message validation in your Express post handler (e.g. make sure messages don't contain certain words) and send an error if it fails.

Try implementing logging in with an email and password using the [Firebase auth API](https://firebase.google.com/docs/auth/web/password-auth). 
You can add users through the Firebase console (under Auth) for testing purposes so you don't need to also implement signing up. You'll need to add a password input to `public/index.html` next to the username input (which will now be email input), and modify the username listener in `public/javascripts/chat.js` to call `firebase.auth().signInWithEmailAndPassword(email, password)`.

Deploy your server to Heroku by [following these instructions](https://devcenter.heroku.com/articles/getting-started-with-nodejs#introduction) so that other people can chat with you. 